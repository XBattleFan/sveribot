(ns de.sveri.gio.core
  (:require [reagent.core :as reagent :refer [atom]]
            [de.sveri.gio.helper :as h]
            [taoensso.sente :as sente]
            [de.sveri.gio.sente-helper :as sh]
            [de.sveri.gio.controls :as contr]))


;;; Some non-DB state
(def state (atom {}))
(def game-info-state (atom {}))
;{:context-maps [] :width 19 :player-index 0}))





(defn get-bg-color [context-map player-id]
  (get contr/player-id-to-color-map (:terrain context-map)))

(defn get-cell-text [context-map player-id]
  ;(str (:idx context-map) "  "
    (cond
     (not (nil? (:city context-map))) (str " \uD83C\uDF06 " (:army context-map))
     (not (nil? (:general context-map))) (str " \uD83D\uDC51️ " (:army context-map))
     (<= 0 (:terrain context-map)) (:army context-map)
     (= -2 (:terrain context-map)) " ⛰ "
     (= -3 (:terrain context-map)) " \uD83C\uDF2B️ ️️"
     (= -4 (:terrain context-map)) "FO"
     :else (:terrain context-map)))

(defn draw-cell [context-map player-id]
  (let [bg-color (get-bg-color context-map player-id)]
    [:div {:style {:background-color bg-color}} (get-cell-text context-map player-id)]))



(defn draw-row [context-maps player-id]
  (for [context-map context-maps]
       ^{:key (str "cell-" (:idx context-map))} [:div.td (draw-cell context-map player-id)]))

(defn draw-game-map [context-maps width player-id]
  (let [parted-maps (partition width context-maps)]
    [:div.table
     (for [i (range 0 (count parted-maps))]
       ^{:key (str "row-" i)} [:div.tr (draw-row (nth parted-maps i) player-id)])]))


;(defn map-player-id-to-color [player-infos]
;  (mapv (fn [player-info] {(keyword (:player-index player-info))})))

(defn uber [sente-map]
  (let [width (:width @state)
        height (:height @state)
        context-maps (:context-maps @state)
        player-id (:player-index @state)
        player-infos (:player-infos @state)
        player-names (:usernames @game-info-state)
        replay-id (:replay_id @game-info-state)]
        ;player-id-to-player-coloer (map-player-id-to-color player-infos)]
    [:div
     (contr/game-controls sente-map width height player-names replay-id player-id)
     (contr/draw-scores player-id player-infos)
     (draw-game-map context-maps width player-id)]))





(defn ^:export main []
  (let [sente-map (sh/start-router! state game-info-state)]
    (reagent/render-component (fn [] [uber sente-map]) (h/get-elem "app"))))
