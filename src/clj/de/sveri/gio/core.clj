(ns de.sveri.gio.core
  (:require [clojure.tools.logging :as log]
            [de.sveri.gio.cljccore :as cljc]
            [de.sveri.gio.components.components :refer [prod-system]]
            [com.stuartsierra.component :as component])
  (:gen-class))

(defn -main [& args]
  (alter-var-root #'prod-system component/start)
  (log/info "server started."))
