(ns de.sveri.gio.game.gathering
  (:require [de.sveri.gio.game.graph :as g]
            [de.sveri.gio.game.helper :as h]))


(defn get-from-to-move [{:keys [distance-graph player-index my-general] :as game-info} from-distance context-maps]
  ;(println (g/next-move-for-from-tiles-to-tile (h/get-own-tiles-with-min-size player-index 2 context-maps) my-general distance-graph))
  ;(println "foo")
  (g/next-move-for-from-tiles-to-tile (h/get-own-tiles-with-min-size player-index 2 context-maps) my-general distance-graph))
