(ns de.sveri.gio.game.has-enemies
  (:require [de.sveri.gio.game.graph :as g]
            [de.sveri.gio.game.helper :as h]
            [de.sveri.gio.game.game-state :as gs]))


;(defn get-next-enemy-to-attack [{:keys [has-enemies? has-enemy-generals? player-index] :as game-info} context-maps]
;  (cond
;    has-enemy-generals? (first (:generals game-info))
;
;    (< 0 (count (filter #(contains? % :city) has-enemies?))) (first (filter #(contains? % :city) has-enemies?))
;
;    :else (first (sort-by :army has-enemies?))))



;(defn get-possible-from-tiles [{:keys [player-index default-graph has-enemies? has-enemy-generals? my-general]  :as game-info} to-tile context-maps]
;  (let [army-avg-size (h/get-own-avg-armies player-index context-maps)
;        own-tiles (h/get-own-tiles-with-min-size player-index army-avg-size context-maps)]
;    (vec (filter #(< (/ (:army my-general) 2) (:army %)) own-tiles))))





(defn get-from-to-for-has-enemies [{:keys [player-index distance-graph has-enemies?] :as game-info} context-maps]
  ;(let [paths (g/get-path-from-to @gs/attacking-tile (first generals) game-info context-maps)]
  ;  [(first paths) (second paths)]))
  (let [to-tile (first (sort-by :army has-enemies?))
        paths (g/get-path-from-to @gs/attacking-tile to-tile game-info context-maps)]
    [(first paths) (second paths)]))
    ;(g/next-move-for-from-tiles-to-tile (h/get-own-tiles-with-min-size player-index 10 context-maps) to-tile distance-graph)))



(defn get-from-to-for-defend-my-general [{:keys [distance-graph has-enemies?] :as game-info} context-maps]
  (g/next-move-for-from-to-tiles (h/get-own-tile-with-largest-army game-info 5 context-maps)
                                 has-enemies? distance-graph))


(defn attack-enemy-general [{:keys [player-index distance-graph generals] :as game-info} context-maps]
  (let [paths (g/get-path-from-to @gs/attacking-tile (first generals) game-info context-maps)]
    [(first paths) (second paths)]))
  ;(g/next-move-for-from-tiles-to-tile (h/get-own-tiles-with-min-size player-index 10 context-maps) (first generals) distance-graph)
  ;(g/getfromto)
  ;(g/next-move-for-from-tiles-to-tile (h/get-own-tiles-with-min-size player-index 10 context-maps) (first generals) distance-graph))

