(ns de.sveri.gio.game.defend-general
  (:require [de.sveri.gio.game.graph :as g]
            [de.sveri.gio.game.helper :as h]))



;(defn get-enemy-closest-to-general [{:keys [player-index my-general default-graph] :as game-info} context-maps]
;  (let [graph (g/generate-graph game-info (:defend-general g/weights) context-maps)
;        enemies (h/get-enemies player-index context-maps)
;        paths-from-to (g/get-paths-from-to-tiles-with-distance my-general enemies graph)
;        sorted-paths (sort-by second paths-from-to)]
;    (-> sorted-paths ffirst last)))





;(defn get-possible-from-tiles [{:keys [player-index default-graph has-enemies? has-enemy-generals? my-general]  :as game-info} to-tile context-maps]
;  (let [army-avg-size (h/get-own-avg-armies player-index context-maps)
;        own-tiles (h/get-own-tiles-with-min-size player-index army-avg-size context-maps)]
;    own-tiles))


;(defn get-from-to-for-general-under-attack [{:keys [default-graph] :as game-info} context-maps]
;  (let [to-tile (get-enemy-closest-to-general game-info context-maps)
;        own-tiles (get-possible-from-tiles game-info to-tile context-maps)
;        paths-from-to (g/get-paths-from-tiles-to-with-distance own-tiles to-tile default-graph)
;        sorted-paths (sort-by second paths-from-to)
;        from-tile (-> sorted-paths ffirst first)
;        paths (g/get-path-from-to from-tile to-tile game-info context-maps)]
;    [(first paths) (second paths)]))
