(ns de.sveri.gio.game.ws-helper)


(defn send-game-update-data [send-fn conn-uids width height context-maps player-index player-infos]
  (when (and send-fn conn-uids)
    (doseq [uid (:any @conn-uids)]
      (send-fn uid [:game/context-maps {:width width :height height :context-maps context-maps
                                        :player-index player-index :player-infos player-infos}]))))


;{:replay_id rx-tYngKx, :usernames [Anonymous [Bot] Timing Bot], :playerIndex 0, :chat_room game_1487092089269SLCm27mnYQq9ug9iAAe-}
(defn send-game-started [send-fn conn-uids game-info]
  (when (and send-fn conn-uids)
    (doseq [uid (:any @conn-uids)]
      (send-fn uid [:game/game-started game-info]))))

