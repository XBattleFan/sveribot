(ns de.sveri.gio.game.logic
  (:require [clojure.data.json :as json]
            [clojure.spec :as s]
            [cuerdas.core :as cuer]
            [de.sveri.gio.game.graph :as g]
            [de.sveri.gio.game.ws-helper :as wsh]
            [de.sveri.gio.game.helper :refer [MOUNTAIN EMPTY] :as h]
            [de.sveri.gio.game.base-building :as bb]
            [de.sveri.gio.game.defend-general :as dg]
            [de.sveri.gio.game.has-enemies :as he]
            [de.sveri.gio.game.game-state :as gs]
            [de.sveri.gio.game.gathering :as gath])
  (:import (java.lang Math)))


(declare build-contextualized-map gather-game-information)


(def game-information {})


(def player-index nil)

(def game-data (atom {:generals [] :cities [] :game-map []}))





(defn patch [old diff]
  (let [out (atom [])
        i (atom 0)]
    (while (< @i (count diff))
      (when (get diff @i)
        (reset! out (vec (concat @out (h/subvec-to-end old (count @out) (+ (count @out) (get diff @i)))))))
      (swap! i inc)
      (when (and (< @i (count diff)) (get diff @i))
        (reset! out (vec (concat @out (h/subvec-to-end diff (+ 1 @i) (+ 1 @i (get diff @i))))))
        (swap! i #(+ % (get diff %))))
      (swap! i inc))
    @out))


;{:attackIndex 0,
; :cities_diff [0],
; :generals [104 -1],
; :scores
;              [{:tiles 1, :total 34, :i 0, :dead false}
;               {:tiles 1, :total 34, :i 1, :dead false}],
; :turn 64,
; :map_diff [106 1 34 777]}




(defn make-next-move [{:keys [state] :as game-info} socket context-maps]
  (let [from-to (cond
                  (= :gather-for-my-general state) (gath/get-from-to-move game-info 7 context-maps)
                  (= :defend-my-general state) (he/get-from-to-for-defend-my-general game-info context-maps)
                  (= :has-enemy-generals state) (he/attack-enemy-general game-info context-maps)
                  (= :attack-enemy state) (he/get-from-to-for-has-enemies game-info context-maps)
                  (= :base-building state) (bb/get-from-to-for-build-base game-info context-maps))
        from-tile (first from-to)
        to-tile (second from-to)
        move-from-to-vec [(str (:idx from-tile)) (str (:idx to-tile)) (if (and (:has-enemies? game-info) (contains? from-tile :general)) "is50" "")]]
    (when (empty? from-to) (println state " - " from-to))
    (.emit socket "attack" (into-array move-from-to-vec))))






;;scores
;              [{:tiles 1, :total 34, :i 0, :dead false}
;               {:tiles 1, :total 34, :i 1, :dead false}],
;
(defn collect-player-infos [scores player-index]
  (let [player-maps (mapv (fn [score] (merge score
                                             {:player-index (:i score)
                                              :is-me?       (if (= player-index (:i score)) true false)}))
                          scores)]
    (vec (sort-by :player-index player-maps))))


; 25 seconds = 50 turns = 50 possible moves,
; every 50th turn the normal tile will add one army

(defn handle-update [data socket send-fn conn-uids]
  (try
    ;(time
    (let [edn-data (-> data first .toString (json/read-str :key-fn keyword))]
      (swap! game-data #(assoc % :cities (patch (:cities %) (:cities_diff edn-data))))
      (swap! game-data #(assoc % :game-map (patch (:game-map %) (:map_diff edn-data))))
      (swap! game-data #(assoc % :generals (:generals edn-data)))
      (let [width (-> @game-data :game-map first)
            height (-> @game-data :game-map second)
            size (* width height)
            armies (h/subvec-to-end (:game-map @game-data) 2 (+ size 2))
            terrain (subvec (:game-map @game-data) (+ size 2))
            context-maps (build-contextualized-map armies terrain (:cities @game-data) (:generals edn-data))
            player-infos (collect-player-infos (:scores edn-data) player-index)
            game-info (gather-game-information {:width width :height height :player-index player-index :turn (:turn edn-data)}
                                               (:generals edn-data) context-maps)]
        (make-next-move game-info socket context-maps)
        (wsh/send-game-update-data send-fn conn-uids width height context-maps player-index player-infos)))
    (catch Exception e (.printStackTrace e))))




(s/fdef build-contextualized-map :ret ::g/context-maps)
(defn build-contextualized-map [armies terrain cities generals]
  (reduce
    (fn [acc idx]
      (conj acc (merge (if (some #{idx} generals) {:general true} {})
                       (if (some #{idx} cities) {:city true} {})
                       {:army (get armies idx) :terrain (get terrain idx) :idx idx})))
    []
    (range 0 (count terrain))))




(defn gather-game-information [game-info generals context-maps]
  (let [cleaned-generals (filter pos-int? generals)
        enemy-generals (h/subvec-to-end cleaned-generals 1 (count cleaned-generals))
        has-enemies? (h/has-enemies? player-index context-maps)
        my-tiles (h/get-own-tiles player-index context-maps)
        pre-map (merge game-info
                       {:has-enemies?        has-enemies?
                        :generals            (h/get-context-maps-for-indexes enemy-generals context-maps)
                        :my-general          (first (filter #(h/is-my-general? % player-index) context-maps))
                        :has-enemy-generals? (< 0 (count enemy-generals))
                        :default-graph       (g/generate-graph game-info (:default g/weights) context-maps)
                        :distance-graph      (g/generate-graph game-info (:distance g/weights) context-maps)
                        :my-tiles            my-tiles
                        :can-take-city?      (< 1000 (reduce + (mapv :army my-tiles)))})]
    ;(println (gs/get-game-state pre-map context-maps))
    (merge pre-map
           {:state (gs/get-game-state pre-map context-maps)})))
