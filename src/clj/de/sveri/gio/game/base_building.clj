(ns de.sveri.gio.game.base-building
  (:require [de.sveri.gio.game.graph :as g]
            [de.sveri.gio.game.helper :as h]))


(def should-print? (atom true))

(defn path-from-inner-tile [from-tile to-tile {:keys [default-graph] :as game-info} context-maps]
  (let [from-tile (h/get-own-tile-with-largest-army game-info 2 context-maps)]
    (g/next-move-for-from-to-tiles from-tile (subvec (h/get-empty-tiles context-maps) 0 3) default-graph)))


(defn path-from-outer-tile [from-tile to-tile game-info context-maps]
  (let [paths (g/get-path-from-to from-tile to-tile game-info context-maps)]
    [(first paths) (second paths)]))

(defn get-from-to-for-build-base [game-info context-maps]
  (let [my-outer-tiles (h/get-my-outer-tiles game-info context-maps)
        from-tile (first (reverse (sort-by :army my-outer-tiles)))
        to-tile (rand-nth (h/get-empty-non-city-neighbors from-tile game-info context-maps))]
    (if (<= (:army from-tile) 1)
      (path-from-inner-tile from-tile to-tile game-info context-maps)
      (path-from-outer-tile from-tile to-tile game-info context-maps))))
