(ns de.sveri.gio.user
  (:require [system.repl :refer [set-init! start reset]]
            [de.sveri.gio.components.components :refer [dev-system]]))

(defn start-dev-system []
  (start))

(set-init! #'dev-system)
