(ns de.sveri.gio.routes.game
  (:require [compojure.core :refer [routes GET POST defroutes]]
            [ring.util.response :refer [response status]]))


;(defn game-routes [{:keys [websockets]}]
;  (routes
;    (GET "/stockfighter" [] (index-page))))


(defn ws-routes [{:keys [websockets]}]
  (routes (GET "/gio/ws" req ((:ajax-get-or-ws-handshake-fn websockets) req))
          (POST "/gio/ws" req ((:ajax-post-fn websockets) req))))
