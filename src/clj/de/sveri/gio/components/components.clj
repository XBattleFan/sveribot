(ns de.sveri.gio.components.components
  (:require
    [com.stuartsierra.component :as component]
    [de.sveri.gio.components.selmer :as selm]
    [de.sveri.gio.components.server :refer [new-web-server]]
    [de.sveri.gio.components.handler :refer [new-handler]]
    [de.sveri.gio.components.websockets :as ws]
    [de.sveri.gio.components.game :as game]
    [de.sveri.gio.components.config :as c]
    [de.sveri.gio.components.db :refer [new-db]]))


(defn dev-system []
  (component/system-map
    :config (c/new-config (c/prod-conf-or-dev))
    :websockets (ws/new-websockets)
    :game (component/using (game/new-game) [:websockets])
    :selmer (selm/new-selmer false)
    :db (component/using (new-db) [:config])
    :handler (component/using (new-handler) [:config :db :websockets :game])
    :web (component/using (new-web-server) [:handler :config])))


(defn prod-system []
  (component/system-map
    :config (c/new-config (c/prod-conf-or-dev))
    :selmer (selm/new-selmer true)
    :db (component/using (new-db) [:config])
    :handler (component/using (new-handler) [:config :db])
    :web (component/using (new-web-server) [:handler :config])))
