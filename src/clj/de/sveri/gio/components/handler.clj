(ns de.sveri.gio.components.handler
  (:require [compojure.core :refer [defroutes routes wrap-routes]]
            [noir.response :refer [redirect]]
            [noir.util.middleware :refer [app-handler]]
            [ring.middleware.defaults :refer [site-defaults]]
            [ring.middleware.file-info :refer [wrap-file-info]]
            [ring.middleware.file :refer [wrap-file]]
            [ring.middleware.params :refer [wrap-params]]
            [ring.middleware.keyword-params :refer [wrap-keyword-params]]
            [compojure.route :as route]
            [com.stuartsierra.component :as comp]
            [de.sveri.gio.routes.home :refer [home-routes]]
            [de.sveri.gio.routes.game :refer [ws-routes]]
            [de.sveri.gio.routes.cc :refer [cc-routes]]
            [de.sveri.gio.routes.user :refer [user-routes registration-routes]]
            [de.sveri.gio.middleware :refer [load-middleware]]))

(defroutes base-routes
  (route/resources "/")
  (route/not-found "Not Found"))

;; timeout sessions after 30 minutes
(def session-defaults
  {:timeout (* 15 60 30)
   :timeout-response (redirect "/")})

(defn- mk-defaults
       "set to true to enable XSS protection"
       [xss-protection?]
       (-> site-defaults
           (update-in [:session] merge session-defaults)
           (assoc-in [:security :anti-forgery] xss-protection?)))

(defn get-handler [config locale {:keys [db]} websockets {:keys [socket]}]
  (routes
    (-> (ws-routes websockets)
        (wrap-routes wrap-params)
        (wrap-routes wrap-keyword-params))
    (-> (app-handler
          (into [] (concat (when (:registration-allowed? config) [(registration-routes config db)])
                           ;; add your application routes here
                           [(cc-routes config) home-routes
                            (user-routes config db) base-routes]))
          ;; add custom middleware here
          :middleware (load-middleware config)
          :ring-defaults (mk-defaults false)
          ;; add access rules here
          :access-rules []
          ;; serialize/deserialize the following data formats
          ;; available formats:
          ;; :json :json-kw :yaml :yaml-kw :edn :yaml-in-html
          :formats [:json-kw :edn :transit-json])
        ; Makes static assets in $PROJECT_DIR/resources/public/ available.
        (wrap-file "resources")
        ; Content-Type, Content-Length, and Last Modified headers for files in body
        (wrap-file-info))))

(defrecord Handler [config locale db websockets game]
  comp/Lifecycle
  (start [comp]
    (assoc comp :handler (get-handler (:config config) locale db websockets game)))
  (stop [comp] comp))


(defn new-handler []
  (map->Handler {}))
