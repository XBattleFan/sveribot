(ns de.sveri.gio.components.game
  (:require [taoensso.sente :as sente]
            [taoensso.sente.server-adapters.immutant :refer (get-sch-adapter)]
            [com.stuartsierra.component :as component]
            [de.sveri.gio.game.core :as gc]))

(defmulti event-msg-handler (fn [event _] (:id event)))

(defmethod event-msg-handler :default
  [{:keys [event]} _]
  (println "Unhandled event: %s" event))

(defmethod event-msg-handler :chsk/ws-ping [_ _])

(defmethod event-msg-handler :game.core/start-game
  [{:keys [?data]} ws]
  (gc/start-game  ws))

(defmethod event-msg-handler :game.core/join-1v1
  [_ ws]
  (gc/join-1v1 ws))

(defmethod event-msg-handler :game.core/join-ffa
  [_ ws]
  (gc/join-ffa ws))

(defmethod event-msg-handler :game.core/autojoin-on
  [_ ws]
  (gc/autojoin true))

(defmethod event-msg-handler :game.core/autojoin-off
  [_ ws]
  (gc/autojoin false))

(defmethod event-msg-handler :game.core/stop-game [_ ws]
  (gc/stop ws))

(defmethod event-msg-handler :game.core/stars-and-rank [_ _]
  (gc/ask-for-stars-and-rank))
;(match ?data
;       [:game.core/start-game] (println "game started")))


(defn start-listening-to-events [ws]
  (sente/start-chsk-router! (:ch-recv ws) (fn [event] (event-msg-handler event ws))))


(defrecord Game [websockets]
  component/Lifecycle
  (start [component]
    (let [ws (:websockets websockets)
          ;_ (gc/connect ws)
          sente-listener-fn (start-listening-to-events ws)]
      ;(assoc component :sente-listener-fn sente-listener-fn)))
      component))
  (stop [component]
    (gc/stop (:websockets websockets))
    component))
    ;(when-let [socket (:game component)] (gc/stop) (Thread/sleep 500))
    ;(dissoc component :game :sente-listener-fn)))

(defn new-game []
  (map->Game {}))
