(ns de.sveri.gio.components.websockets
  (:require [taoensso.sente :as sente]
            [taoensso.sente.server-adapters.immutant :refer (get-sch-adapter)]
            [clojure.core.match :refer [match]]
            [com.stuartsierra.component :as component]))



(defn create-socket []
  (let [ws (sente/make-channel-socket! (get-sch-adapter) {})]
    ws))


(defrecord Websockets []
  component/Lifecycle
  (start [component]
      (assoc component :websockets (create-socket)))
  (stop [component] (dissoc component :websockets)))

(defn new-websockets []
  (map->Websockets {}))
